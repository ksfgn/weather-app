import axios from 'axios';

export const $api = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  timeout: import.meta.env.DEV ? 20000 : 60000,
});
